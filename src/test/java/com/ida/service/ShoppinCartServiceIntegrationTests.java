//package com.ida.service;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertNotNull;
//import static org.junit.Assert.assertTrue;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import be.ida.model.ShoppingCart;
//import be.ida.repository.ShoppingCartRepository;
//
///*
// * Cart id's are 1 digit, f.e. 1
// * User id's are 2 digit, f.e. 11
// * Product id's are 3 digits,  111
// */
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class ShoppinCartServiceIntegrationTests {
//
//	@Autowired
//	ShoppingCartRepository shoppingCartRepository;
//	ShoppingCart cart;
//
//	@Before
//	public void setUp() {
//		cart = new ShoppingCart(1, 11);
//		cart.addProduct(111);
//		this.shoppingCartRepository.save(cart);
//
//	}
//
//	// Create Shopping Cart tests
//
//	@Test
//	public void createShoppingCartTest() {
//		this.shoppingCartRepository.save(new ShoppingCart(2, 22));
//		assertNotNull(this.shoppingCartRepository.findById(2));
//	}
//
//	@Test
//	public void failsOnDuplicateId() {
//		int initSize = this.shoppingCartRepository.findAll().size();
//		this.shoppingCartRepository.save(new ShoppingCart(1, 22));
//		assertEquals(this.shoppingCartRepository.findAll().size(), initSize);
//	}
//
//	// Add and Remove tests
//
//	@Test
//	public void addToShoppingCartTest() throws InterruptedException {
//		int initSize = this.shoppingCartRepository.findById(1).getProducts().size();
//		this.shoppingCartRepository.findById(1).addProduct(222);
//		assertEquals(this.shoppingCartRepository.findById(1).getProducts().size(), initSize + 1);
//	}
//
//	@Test
//	public void addDuplicateToShoppingCartTest() {
//		int initSize = this.shoppingCartRepository.findById(1).getProducts().size();
//		this.shoppingCartRepository.findById(1).addProduct(111);
//		assertEquals(this.shoppingCartRepository.findById(1).getProducts().size(), initSize + 1);
//	}
//
//	@Test
//	public void removeFromShoppingCartTest() {
//		int initSize = this.shoppingCartRepository.findById(1).getProducts().size();
//		this.shoppingCartRepository.findById(1).removeProduct(111);
//		assertEquals(this.shoppingCartRepository.findById(1).getProducts().size(), initSize - 1);
//	}
//
//	@Test
//	public void removeNothingFromShoppingCartTest() {
//		int initSize = this.shoppingCartRepository.findById(1).getProducts().size();
//		this.shoppingCartRepository.findById(1).removeProduct(222);
//		assertEquals(this.shoppingCartRepository.findById(1).getProducts().size(), initSize);
//	}
//
//	// Other Tests
//
//	@Test
//	public void getShoppingCart() {
//		ShoppingCart cart2 = this.shoppingCartRepository.findById(1);
//		assertTrue(cart.getId() == cart2.getId());
//	}
//
//	@Test
//	public void getShoppingCartOnUser() {
//		ShoppingCart cart2 = this.shoppingCartRepository.findByUserId(11);
//		assertTrue(cart.getId() == cart2.getId());
//	}
//
//	@Test
//	public void Checkout() {
//		// TODO
//	}
//}
