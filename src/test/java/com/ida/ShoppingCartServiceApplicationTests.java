//package com.ida;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class ShoppingCartServiceApplicationTests {
//
//	@Autowired
//	private WebApplicationContext context;
//	private MockMvc mvc;
//
//	@Before
//	public void setUp() {
//		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
//	}
//
//	private void addCart() throws Exception {
//		this.mvc.perform(get("/create/1/12"));
//	}
//
//	private void addContent() throws Exception {
//		this.mvc.perform(get("/add/12/123"));
//	}
//
//	// Test mappings //
//
//	@Test
//	public void testDefaultUrl() throws Exception {
//		this.mvc.perform(get("/").contentType(MediaType.ALL)).andExpect(status().isOk())
//				.andExpect(content().string(org.hamcrest.Matchers.containsString("Mappings")));
//	}
//
//	@Test
//	public void testCreateCart() throws Exception {
//		this.mvc.perform(get("/create/1/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
//	}
//
//	@Test
//	public void testGetProducts() throws Exception {
//		addCart();
//		addContent();
//		this.mvc.perform(get("/get/12").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
//	}
//
//	@Test
//	public void testAddProduct() throws Exception {
//		addCart();
//		this.mvc.perform(get("/add/12/123").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
//	}
//
//	@Test
//	public void testRemoveProduct() throws Exception {
//		addCart();
//		addContent();
//		this.mvc.perform(get("/remove/12/123").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
//	}
//
//	@Test
//	public void testCannotRemoveUnexistingProduct() throws Exception {
//		addCart();
//		addContent();
//		this.mvc.perform(get("/remove/12/321").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
//	}
//}
