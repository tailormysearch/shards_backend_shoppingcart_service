package be.ida.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.ida.dto.ProductPageDTO;

import be.ida.model.ShoppingCart;
import be.ida.service.ShoppingCartService;

@RestController
public class ShoppingCartController {
	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private ShoppingCartService shoppingCartService;

	@Autowired
	private RequestMappingHandlerMapping requestMappingHandlerMapping;

	@GetMapping("/")
	public String defaultGreeting() {
		String greeting = "<h1>Shopping Cart Service</h1><p>Mappings:</p><ul>";
		for (RequestMappingInfo m : requestMappingHandlerMapping.getHandlerMethods().keySet()) {
			greeting += "<li>" + m + "</li>";
		}
		greeting += "</ul>";
		return greeting;
	}
	
	/*
	 * Mappings for CRUD functions
	 */

	@RequestMapping(path="/create/{uid}/{id}", method=RequestMethod.POST)
	public void createCart(@PathVariable int id, @PathVariable int uid) {
		logger.info("Create Cart for user " + uid + " with id " + id);
		shoppingCartService.createShoppingCart(id, uid);
	}

	@RequestMapping(path="/get/{id}", method=RequestMethod.GET)
	public ProductPageDTO getProducts(@PathVariable int id) {
		logger.info("Get list of productids in cart with id " + id);
		return shoppingCartService.getProducts(id);
	}

	@RequestMapping(path="/add/{id}/{pid}", method=RequestMethod.POST)
	public void addProduct(@PathVariable int id, @PathVariable int pid) {
		logger.info("Added product " + pid + " to cart with id " + id);
		shoppingCartService.addToShoppingCart(id, pid);
	}

	@RequestMapping(path="/remove/{id}/{pid}", method=RequestMethod.DELETE)
	public void removeProduct(@PathVariable int id, @PathVariable int pid) {
		logger.info("Remove product " + pid + " from cart with id " + id);
		shoppingCartService.removeFromShoppingCart(id, pid);
	}

	@RequestMapping(path="/cartfromid/{id}", method=RequestMethod.GET)
	public ShoppingCart getCartFromId(@PathVariable int id) {
		logger.info("get cart object from id " + id);
		return shoppingCartService.getShoppingCart(id);
	}

	@RequestMapping(path="/cartfromuser/{uid}", method=RequestMethod.GET)
	public ShoppingCart getCartFromUser(@PathVariable int uid) {
		logger.info("get cart object from user with UID " + uid);
		return shoppingCartService.getShoppingCartOnUser(uid);
	}
	
	@RequestMapping(path="/checkout/{id}", method=RequestMethod.DELETE)
	public void checkOut(@PathVariable int id, @PathVariable int pid) {
		logger.info(id+ "is checked out");
		shoppingCartService.checkout(id);
	}
	
	@RequestMapping(path="/carts", method=RequestMethod.GET)
	public List<ShoppingCart> getCarts() {
		logger.info("get all Carts");
		return shoppingCartService.getCarts();
	}
}
