package be.ida.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import be.ida.model.ShoppingCart;

@Repository
public interface ShoppingCartRepository extends CrudRepository<ShoppingCart, Integer> {
	ShoppingCart findById(int id);
	ShoppingCart findByUserId(int userId);

	List<ShoppingCart> findAll();
}
