package be.ida.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ShoppingCart {

	@Id
	private int id;
	private int userId;
	private ArrayList<Integer> productIds = new ArrayList<>();

	public ShoppingCart(int id, int userId) {
		this.id = id;
		this.userId = userId;
	}

	public ShoppingCart() {
	}

	public void addProduct(int productId) {
		productIds.add(productId);
	}

	public void removeProduct(int productId) {
		for (int i = 0; i < productIds.size(); i++) {
			if (productIds.get(i) == productId) {
				productIds.remove(i);
				break; // only remove first instance found
			}
		}
	}

	public void checkout() {
		
	}

	// Getters

	public int getId() {
		return id;
	}

	public int getuserId() {
		return userId;
	}

	public List<Integer> getProducts() {
		return productIds;
	}

	// Setters

	public void setId(int id) {
		this.id = id;
	}

	public void setuserId(int userId) {
		this.userId = userId;
	}

	public void setProducts(ArrayList<Integer> productIds) {
		this.productIds = productIds;
	}

	@Override
	public String toString() {
		return String.format("Cart[id=%d, userId='%s']", id, userId);
	}
}
