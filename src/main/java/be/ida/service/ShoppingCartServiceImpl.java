package be.ida.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ida.dto.ProductDTO;
import com.ida.dto.ProductPageDTO;

import be.ida.model.ShoppingCart;
import be.ida.repository.ShoppingCartRepository;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {

	@Autowired
	private ShoppingCartRepository shoppingCartRepository;


	@Override
	public void createShoppingCart(int id, int userId) {
		for (ShoppingCart cart : shoppingCartRepository.findAll()) {
			if (cart.getuserId() == userId) {
				return;
			}
		}
		ShoppingCart cart = new ShoppingCart(id, userId);
		shoppingCartRepository.save(cart);
	}

	@Override
	public void addToShoppingCart(int id, int productId) {
		ShoppingCart cart = shoppingCartRepository.findById(id);
		cart.addProduct(productId);
		shoppingCartRepository.save(cart);
	}

	@Override
	public void removeFromShoppingCart(int id, int productId) {
		ShoppingCart cart = shoppingCartRepository.findById(id);
		cart.removeProduct(productId);
		shoppingCartRepository.save(cart);
	}

	@Override
	public ShoppingCart getShoppingCart(int id) {
		return shoppingCartRepository.findById(id);
	}

	@Override
	public ShoppingCart getShoppingCartOnUser(int userId) {
		return shoppingCartRepository.findByUserId(userId);
	}

	@Override
	public void checkout(int id) {
		shoppingCartRepository.delete(id);
	}

	@Override
	public ProductPageDTO getProducts(int id) {
		RestTemplate restTemplate = new RestTemplate();
		List<Integer> productIds = shoppingCartRepository.findById(id).getProducts();
		ProductPageDTO page = new ProductPageDTO();
		for(Integer productId : productIds) {
			page.add(restTemplate.getForObject("http://shards.eu-west-1.elasticbeanstalk.com/product-api/product/"+productId, ProductDTO.class));
		}
		return page;
	}

	@Override
	public List<ShoppingCart> getCarts() {
		return shoppingCartRepository.findAll();
	}
	
	
}
