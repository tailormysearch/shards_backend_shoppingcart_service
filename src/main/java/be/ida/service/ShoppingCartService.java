package be.ida.service;

import java.util.List;

import com.ida.dto.ProductPageDTO;

import be.ida.model.ShoppingCart;

public interface ShoppingCartService {

	public void createShoppingCart(int id, int userId);

	public void addToShoppingCart(int id, int productId);

	public void removeFromShoppingCart(int id, int productId);

	public void checkout(int id);

	public ShoppingCart getShoppingCart(int id);

	public ShoppingCart getShoppingCartOnUser(int userId);

	public ProductPageDTO getProducts(int id);
	
	public List<ShoppingCart> getCarts();
}